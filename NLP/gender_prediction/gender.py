# -*- coding: utf-8 -*-
import random, nltk, matplotlib
from nltk.corpus import names
import matplotlib.pyplot as plt


matplotlib.style.use("ggplot")
#%matplotlib inline

name_cfd = nltk.ConditionalFreqDist( (fileid, name[-2:]) for fileid in names.fileids() for name in names.words(fileid) )
plt.figure( figsize=(50,10) )
#name_cfd.plot()


def name_features(name):
	return {"pair" : name[-2:]}

name_list = [ (name, "male") for name in names.words('male.txt') ] + [ (name, "female") for name in names.words('female.txt') ]
random.shuffle(name_list)
features = [ (name_features(name), gender) for (name, gender) in name_list ]

training_set = features[:(len(features)/2)]
testing_set = features[(len(features)/2):]

classifier = nltk.NaiveBayesClassifier.train(training_set)
male_names = names.words("male.txt")

print("Carmello" in male_names)
print( classifier.classify( name_features("Carmello") ) )
print( nltk.classify.accuracy(classifier, testing_set) )




