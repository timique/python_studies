# -*- coding: utf-8 -*-
import nltk, csv, sys, os, webbrowser
import numpy as np
from os import listdir
from os.path import isfile, join

path_resources = 'resources'
path_source = 'data'
path_result = 'result'
files = []
negative = []
positive = []


def writeResult(analysis):
	html_head = """<!DOCTYPE html>
<html>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<title>%s</title>
<body>
<article>
	<h1 style="text-align:center;">Analysis of the file %s</h1>
	<ol>""" % (analysis['filename'], analysis['filename'])
	html_body = ""
	html_bottom = """</ol>
</article>
</body>
</html>"""
	
	filename = path_result + os.path.sep + analysis['filename'] + '.html'
	try:
		f = open(filename, 'w')
	except:
		print("ERROR!\n\tFailed to create the file: \n\t\t" + os.path.realpath(filename))
	else:
		for data_item in analysis['data']:
			html_body += """
		<li>
			<h2>Score: %s</h2>
			<details>
				<summary style="color:#6b6b6b;">%s</summary>
				<p>%s</p>
			</details>
		</li>""" % (data_item['score'], data_item['teaser'], data_item['text'])
		try:
			f.write(html_head + html_body + html_bottom)
		finally:
			f.close()
		webbrowser.open('file://' + os.path.realpath(filename))


def sentiment(text):
	sentences_score = []
	text_sentences = nltk.sent_tokenize(text)
	for sentence in text_sentences:
		n_count = 0
		p_count = 0
		sentence_words = nltk.word_tokenize(sentence)
		for word in sentence_words:
			for item in positive:
				if(word == item[0]):
					p_count += 1
			for item in negative:
				if(word == item[0]):
					n_count += 1
		if(p_count > 0 and n_count == 0):
			sentences_score.append(1)
		elif(n_count%2 > 0):
			sentences_score.append(-1)
		elif(n_count%2 == 0 and n_count > 0):
			sentences_score.append(1)
		else:
			sentences_score.append(0)
	return np.average(sentences_score)


def analyseFile(filename):
	analysis = {'filename':filename, 'data':[]}
	filename = path_source + os.path.sep + filename
	
	try:
		f = open(filename, 'rb')
	except:
		print("ERROR!\n\tFailed to read the file: \n\t\t" + os.path.realpath(filename))
		return analysis
	else:
		try:
			reader = csv.reader(f)
		except:
			print("ERROR!\n\tFailed to read tdata from the file: \n\t\t" + os.path.realpath(filename))
		else:
			for row in reader:
				if len(row) > 0:
					result = {'score': sentiment(str(row[0])), 'teaser': row[0][:250]+'...', 'text': row[0]}
					analysis['data'].append(result)
			return analysis
		finally:
			f.close()
	return analysis




try:
	with open(path_resources + os.path.sep + 'words-negative.csv', 'rb') as wn_file:
		wn_reader = csv.reader(wn_file)
		for wn_reader_row in wn_reader:
			negative.append(wn_reader_row)
except:
	sys.exit("FATAL ERROR: cannot read file " + os.path.realpath(path_resources + os.path.sep + 'words-negative.csv'))

try:
	with open(path_resources + os.path.sep + 'words-positive.csv', 'rb') as wp_file:
		wp_reader = csv.reader(wp_file)
		for wp_reader_row in wp_reader:
			positive.append(wp_reader_row)
except:
	sys.exit("FATAL ERROR: cannot read file " + os.path.realpath(path_resources + os.path.sep + 'words-positive.csv'))

try:
	source_files = [f for f in listdir(path_source) if isfile(join(path_source, f))]
except:
	sys.exit("FATAL ERROR: cannot read the directory " + path_source)

if not len(source_files) > 0:
	sys.exit("No files to process")


for source_file in source_files:
	analysis = analyseFile(source_file)
	if len(analysis['data']) > 0:
		writeResult(analysis)

