# -*- coding: utf-8 -*-
import nltk, csv, sys, os, webbrowser
from nltk.sentiment.vader import SentimentIntensityAnalyzer
from os import listdir
from os.path import isfile, join

path_source = 'data'
path_result = 'result'
files = []


def writeResult(analysis):
	html_head = """<!DOCTYPE html>
<html>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<title>%s</title>
<body>
<article>
	<h1 style="text-align:center;">Analysis of the file %s with Twython</h1>
	<ol>""" % (analysis['filename'], analysis['filename'])
	html_body = ""
	html_bottom = """</ol>
</article>
</body>
</html>"""
	
	filename = path_result + os.path.sep + analysis['filename'] + '_twython.html'
	try:
		f = open(filename, 'w')
	except:
		print("ERROR!\n\tFailed to create the file: \n\t\t" + os.path.realpath(filename))
	else:
		for data_item in analysis['data']:
			html_body += """
		<li>
			<h2>Score: %s</h2>
			<details>
				<summary style="color:#6b6b6b;">%s</summary>
				<p>%s</p>
			</details>
		</li>""" % (data_item['score'], data_item['teaser'], data_item['text'])
		try:
			f.write(html_head + html_body + html_bottom)
		finally:
			f.close()
		webbrowser.open('file://' + os.path.realpath(filename))


def analyseFile(filename):
	sid = SentimentIntensityAnalyzer()
	analysis = {'filename':filename, 'data':[]}
	filename = path_source + os.path.sep + filename
	
	try:
		f = open(filename, 'rb')
	except:
		print("ERROR!\n\tFailed to read the file: \n\t\t" + os.path.realpath(filename))
		return analysis
	else:
		try:
			reader = csv.reader(f)
		except:
			print("ERROR!\n\tFailed to read tdata from he file: \n\t\t" + os.path.realpath(filename))
		else:
			for row in reader:
				if len(row) > 0:
					result = {'score': sid.polarity_scores(row[0])['compound'], 'teaser': row[0][:250]+'...', 'text': row[0]}
					analysis['data'].append(result)
			return analysis
		finally:
			f.close()
	return analysis





try:
	source_files = [f for f in listdir(path_source) if isfile(join(path_source, f))]
except:
	sys.exit("FATAL ERROR: cannot read the directory " + path_source)

if not len(source_files) > 0:
	sys.exit("No files to process")

for source_file in source_files:
	analysis = analyseFile(source_file)
	if len(analysis['data']) > 0:
		writeResult(analysis)
	#print analysis
