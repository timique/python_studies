from sklearn.feature_extraction.text import TfidfVectorizer
from sklearn.decomposition import NMF
import pandas as pd
import numpy as np
import  sys, os
from os import listdir
from os.path import isfile, join

path_source = 'source'
dataset = []


try:
	dataset = [ (open(path_source + os.path.sep + f).read()) for f in listdir(path_source) if isfile(join(path_source, f))]
except:
	sys.exit("FATAL ERROR: cannot read the directory " + path_source)


vectorizer = TfidfVectorizer(min_df=1, max_features=100, stop_words="english")
tfidf = vectorizer.fit_transform(dataset)
nmf = NMF(n_components=10, random_state=1).fit(tfidf)
feature_names = vectorizer.get_feature_names()

for topic_idx, topic in enumerate(nmf.components_):
	print("Topic #%d:" %topic_idx)
	print " ".join([feature_names[i] for i in topic.argsort()[:-10 -1:-1]])
	print "\n"

