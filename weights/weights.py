from tkinter import *

window = Tk()
window.wm_title("kgs<->grams<->pounds<->ounces")

btn_state = 0

def weights_convert():
    global btn_state

    if btn_state == 0 :
        entry_kg_value_fl = float(entry_kg_value.get()) if entry_kg_value.get() != '' else 0
        entry_gram_value_fl = float(entry_gram_value.get()) if entry_gram_value.get() != '' else 0
        entry_pound_value_fl = float(entry_pound_value.get()) if entry_pound_value.get() != '' else 0
        entry_ounce_value_fl = float(entry_ounce_value.get()) if entry_ounce_value.get() != '' else 0

        if not entry_kg_value_fl and not entry_gram_value_fl and not entry_pound_value_fl and not entry_ounce_value_fl :
            return False

        if isinstance(entry_kg_value_fl, float) and entry_kg_value_fl > 0 :
            gram = entry_kg_value_fl * 1000
            pound = entry_kg_value_fl * 2.20462
            ounce = entry_kg_value_fl * 35.274

            entry_gram.insert(END, gram)
            entry_pound.insert(END, pound)
            entry_ounce.insert(END, ounce)

        elif isinstance(entry_gram_value_fl, float) and entry_gram_value_fl > 0 :
            kg = entry_gram_value_fl / 1000
            pound = kg * 2.20462
            ounce = kg * 35.274

            entry_kg.insert(END, kg)
            entry_pound.insert(END, pound)
            entry_ounce.insert(END, ounce)

        elif isinstance(entry_pound_value_fl, float) and entry_pound_value_fl > 0 :
            kg = entry_pound_value_fl / 2.20462
            gram = kg * 1000
            ounce = kg * 35.274

            entry_kg.insert(END, kg)
            entry_gram.insert(END, gram)
            entry_ounce.insert(END, ounce)

        elif isinstance(entry_ounce_value_fl, float) and entry_ounce_value_fl > 0 :
            kg = entry_ounce_value_fl / 35.274
            gram = kg * 1000
            pound = kg * 2.20462

            entry_kg.insert(END, kg)
            entry_gram.insert(END, gram)
            entry_pound.insert(END, pound)

        else :
            return False

        b1.configure(text='Clear')
        btn_state = 1
    else :
        b1.configure(text='Convert')
        btn_state = 0
        entry_kg.delete(0, END)
        entry_gram.delete(0, END)
        entry_pound.delete(0, END)
        entry_ounce.delete(0, END)
        #t3.delete(1.0,END)

    return True


entry_kg_value = StringVar()
entry_kg_lable = Label(window, text="kilogram")
entry_kg = Entry(window, textvariable=entry_kg_value)

entry_gram_lable = Label(window, text="gram")
entry_gram_value = StringVar()
entry_gram = Entry(window, textvariable=entry_gram_value)

entry_pound_lable = Label(window, text="pound")
entry_pound_value = StringVar()
entry_pound = Entry(window, textvariable=entry_pound_value)

entry_ounce_lable = Label(window, text="ounce")
entry_ounce_value = StringVar()
entry_ounce = Entry(window, textvariable=entry_ounce_value)

b1 = Button(window, text="Convert", command=weights_convert)


entry_kg_lable.grid(row=0, column=0)
entry_kg.grid(row=0, column=1)
entry_gram_lable.grid(row=0, column=2)
entry_gram.grid(row=0, column=3)
entry_pound_lable.grid(row=1, column=0)
entry_pound.grid(row=1,column=1)
entry_ounce_lable.grid(row=1,column=2)
entry_ounce.grid(row=1, column=3)
b1.grid(row=2, columnspan=4)


window.mainloop()
