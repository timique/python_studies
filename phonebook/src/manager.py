import os, sys
from src import database

class DBManager:
	
	def __init__(self):
		db_dir = "data"
		try:
			os.makedirs(db_dir, exist_ok=True)
		except:
			sys.exit("FATAL ERROR: cannot create Database directory")
		else:
			tables_create = ["CREATE TABLE IF NOT EXISTS phonebook (id INTEGER PRIMARY KEY, surname text, fname text, phone text, info text)"]
			self.DB = database.Database(os.path.join(db_dir, "phonebook.db"), tables_create)


	def insert(self, surname, phone, name='', info=''):
		sql_str  = "INSERT INTO phonebook (surname, fname, phone, info) VALUES ('%s', '%s', '%s', '%s')" % (surname, name, phone, info)
		insert_status = self.DB.insert(sql_str)
		if insert_status["rows_affected"] > 0 and insert_status["last_id"] > -1 :
			return self.DB.view("SELECT * FROM phonebook WHERE id = %d" % insert_status["last_id"])
		else :
			return []
			
	
	def view_search_command(self, search_args={}):
		if not len(search_args) > 0:
			sql_str = "SELECT * FROM phonebook"
		else:
			tuple_args = []
			sql_string = "SELECT * FROM phonebook "
			i = 0
			for search_arg in search_args:
				if not search_args[search_arg] == '':
					if i == 0:
						sql_string += " WHERE %s LIKE '%s' "
					else:
						sql_string += " AND %s LIKE '%s' "
					tuple_args.append(search_arg)
					tuple_args.append('%'+search_args[search_arg]+'%')
					i += 1
			sql_str = sql_string % tuple(tuple_args)

		return sorted(self.DB.view(sql_str), key=lambda k: k['surname'].lower(), reverse=False)
		
		
	def update(self, item_id, surname, phone, name='', info=''):
		sql_str  = "UPDATE phonebook SET surname = '%s', fname = '%s', phone = '%s', info = '%s' WHERE id = %d" % (surname, name, phone, info, item_id)
		update_status = self.DB.update(sql_str)
		if update_status > 0  :
			return self.DB.view("SELECT * FROM phonebook WHERE id = %d" % (item_id,))
		else :
			return []

			
	def delete(self, item_id):
		sql_str  = "DELETE FROM phonebook WHERE id = %d" % (item_id)
		return self.DB.delete(sql_str)
