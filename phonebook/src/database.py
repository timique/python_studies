import sys, sqlite3

class Database:

	def __init__(self, db, tables_create):
		self.tables_create = tables_create	
		self.connection = None
		try:
			self.connection = sqlite3.connect(db)
		except:
			sys.exit("FATAL ERROR: cannot connect to DB")
		else:
			self.connection.row_factory = sqlite3.Row
			self.cursor = self.connection.cursor()
			self.check_db()
			
		
	def check_db(self):
		for tbl in self.tables_create:
			self.cursor.execute(tbl)
		self.connection.commit()
		

	def insert(self, sql):
		rows_affected = 0
		last_id = -1
		try:
			self.cursor.execute(sql)
			self.connection.commit()
			rows_affected = self.cursor.rowcount
			last_id = self.cursor.lastrowid
		except :
			rows_affected = 0
			last_id = -1
		finally:
			return {
				"rows_affected": rows_affected,
				"last_id": last_id
			}
			
	
	def view(self, sql):
		self.cursor.execute(sql)
		rows = self.cursor.fetchall()
		view_result = []
		for row in rows:
			row_dict = {}
			for row_key in row.keys():
				row_dict[row_key] = row[row_key]
			view_result.append(row_dict)
		return view_result
		
		
	def update(self, sql):
		rows_affected = 0
		try:
			test = self.cursor.execute(sql)
			stesta = self.connection.commit()
			rows_affected = self.cursor.rowcount
		except:
			rows_affected = 0
		finally:
			return rows_affected
			
			
	def delete(self, sql):
		rows_affected = 0
		try:
			self.cursor.execute(sql)
			self.connection.commit()
			rows_affected = self.cursor.rowcount
		except:
			rows_affected = 0
		finally:
			return rows_affected
			
        
	def __del__(self):
		if self.connection is not None:
			self.connection.close()
